{
  description = "";

  inputs = {
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-utils, devshell, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system: 
      let
          pkgs = import nixpkgs {
            inherit system;

            overlays = [ devshell.overlay ];
          };
          ipns-utils = pkgs.buildGoModule {
            name = "ipns-utils";
            src = ./.;
            vendorSha256 = "sha256-HndfSJmDnsYExXSdbNLMjuJBYu8eQiEhpC+MyuNP2ks=";
          };
      in rec {
        devShell = pkgs.devshell.mkShell {
          # imports = [ (pkgs.devshell.importTOML ./devshell.toml) ];
          devshell.packages = with pkgs; [
            go
          ];
        };

        packages.default = ipns-utils;
      }
      );
}
